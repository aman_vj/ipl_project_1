async function getUrl(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}
function commonHighChart(
  url,
  chartType,
  title,
  text1,
  text2,
  series_name,
  container_id
) {
  let data = getUrl(url);
  data
    .then(highCharts =>
      Highcharts.chart(container_id, {
        chart: {
          type: chartType
        },
        title: {
          text: title
        },

        subtitle: {
          text: 'Source: IPL.com'
        },
        xAxis: {
          title: {
            text: text1
          },
          categories: Object.keys(highCharts)
        },
        yAxis: {
          title: {
            text: text2
          }
        },
        legend: {
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'middle'
        },
        series: [
          {
            name: series_name,
            data: Object.values(highCharts)
          }
        ],

        responsive: {
          rules: [
            {
              condition: {
                maxWidth: 500
              },
              chartOptions: {
                legend: {
                  layout: 'horizontal',
                  align: 'center',
                  verticalAlign: 'bottom'
                }
              }
            }
          ]
        }
      })
    )
    .catch(error => console.log(`error ${error}`));
}
commonHighChart(
  '../output/matchesPerYear.json',
  'line',
  'Matches Per Year',
  'Year',
  'Matches',
  'Matches Won',
  'container1'
);
commonHighChart(
  '../output/extraRunsConcededPerTeamIn2016.json',
  'bar',
  'Extra Runs Conceded Per Team In 2016',
  'Team',
  'Extra Runs Conceded',
  'Extra Runs Conceded',
  'container3'
);
commonHighChart(
  '../output/top10EconomicalBowlersIn2015.json',
  'column',
  'Top 10 Economical Bowlers In 2015',
  'Bowler',
  'Economy',
  'Economy',
  'container4'
);
const data2 = getUrl('../output/matchesWonPerTeamPerYear.json');
data2
  .then(matches =>
    Highcharts.chart('container2', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Matches Won Per Team Per Year'
      },
      xAxis: {
        title: {
          text: 'Years'
        },
        categories: matchesWonPerYear(matches)
      },
      yAxis: {
        title: {
          text: 'Matches Won'
        }
      },
      series: distinctTeamData(matches),
      legend: {
        align: 'left',
        x: 100,
        verticalAlign: 'top',
        y: 0,
        floating: false,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
      },
      tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
      },
      plotOptions: {
        column: {
          stacking: 'normal',
          dataLabels: {
            enabled: false
          }
        }
      }
    })
  )
  .catch(error => console.log(`error ${error}`));

function matchesWonPerYear(matches) {
  var matchesDataValue = Object.values(matches);
  var years = matchesDataValue.map(year => Object.keys(year));
  years = years.reduce((output, year) => {
    return output.concat(year);
  }, []);
  let uniqueYears = years.reduce((uniqueYear, year) => {
    if (!uniqueYear.includes(year)) {
      uniqueYear.push(year);
    }
    return uniqueYear;
  }, []);
  uniqueYears.sort();
  return uniqueYears;
}
function distinctTeamData(matches) {
  let years = matchesWonPerYear(matches);
  var yearArray = years.reduce((yearArray, year) => {
    yearArray[year] = 0;
    return yearArray;
  }, {});
  var teamAndMatchesWonPerYearArray = Object.entries(matches);
  var teamAndMatchesWonPerYearObject = teamAndMatchesWonPerYearArray.reduce(
    (teamAndMatchesWonPerYearObject, entry) => {
      teamAndMatchesWonPerYearObject[entry[0]] = Object.assign(
        { ...yearArray },
        entry[1]
      );
      return teamAndMatchesWonPerYearObject;
    },
    {}
  );
  var seriesArray = Object.entries(teamAndMatchesWonPerYearObject).reduce(
    (seriesArray, currArr) => {
      seriesArray.push({});
      seriesArray[seriesArray.length - 1]['name'] = currArr[0];
      seriesArray[seriesArray.length - 1]['data'] = Object.values(currArr[1]);
      return seriesArray;
    },
    []
  );
  return seriesArray;
}
