function matchesPerYear(matches) {
  let matchesPerYear = matches.reduce((matchPerYear, match) => {
    if (!matchPerYear[match['season']]) {
      matchPerYear[match['season']] = 1;
    } else {
      matchPerYear[match['season']] += 1;
    }
    return matchPerYear;
  }, {});
  return matchesPerYear;
}

function matchesWonPerTeamPerYear(matches) {
  let matchesWonPerTeamPerYear = matches.reduce((matchesWonByTeam, match) => {
    if (!matchesWonByTeam[match['team1']]) {
      matchesWonByTeam[match['team1']] = {};
    }
    if (!matchesWonByTeam[match['team1']][match['season']]) {
      matchesWonByTeam[match['team1']][match['season']] = 0;
    }
    if (!matchesWonByTeam[match['team2']]) {
      matchesWonByTeam[match['team2']] = {};
    }
    if (!matchesWonByTeam[match['team2']][match['season']]) {
      matchesWonByTeam[match['team2']][match['season']] = 0;
    }
    if (match['winner'] !== '') {
      matchesWonByTeam[match['winner']][match['season']]++;
    }
    return matchesWonByTeam;
  }, {});
  return matchesWonPerTeamPerYear;
}

function extraRunsConcededPerTeamIn2016(matches, deliveryData, season) {
  let deliveries = filterMatchData(matches, deliveryData, season);
  let teamsInSeason = deliveries.reduce((teamInSeason, delivery) => {
    if (!teamInSeason[delivery['bowling_team']]) {
      teamInSeason[delivery['bowling_team']] = parseInt(delivery['extra_runs']);
    } else {
      teamInSeason[delivery['bowling_team']] += parseInt(
        delivery['extra_runs']
      );
    }
    return teamInSeason;
  }, {});
  return teamsInSeason;
}

function top10EconomicalBowlersIn2015(matches, deliveryData, season) {
  let deliveries = filterMatchData(matches, deliveryData, season);
  let bowlersData = deliveries.reduce((bowlers, delivery) => {
    if (!bowlers[delivery['bowler']]) {
      bowlers[delivery['bowler']] = {};
      bowlers[delivery['bowler']]['runs_conceded'] =
        parseInt(delivery['noball_runs']) +
        parseInt(delivery['wide_runs']) +
        parseInt(delivery['batsman_runs']);
      bowlers[delivery['bowler']]['balls'] = 1;
      bowlers[delivery['bowler']]['economy'] =
        (bowlers[delivery['bowler']]['runs_conceded'] /
          bowlers[delivery['bowler']]['balls']) *
        6;
    }
    bowlers[delivery['bowler']]['runs_conceded'] +=
      parseInt(delivery['noball_runs']) +
      parseInt(delivery['wide_runs']) +
      parseInt(delivery['batsman_runs']);
    bowlers[delivery['bowler']]['balls']++;
    bowlers[delivery['bowler']]['economy'] =
      (bowlers[delivery['bowler']]['runs_conceded'] /
        bowlers[delivery['bowler']]['balls']) *
      6;
    return bowlers;
  }, {});
  let top10EconomicalBowlers = Object.entries(bowlersData)
    .sort((a, b) => a[1].economy - b[1].economy)
    .filter(bowlers => bowlers[1]['balls'] > 29) //29 - minimum balls delivered by a bowler to be counted.
    .slice(0, 10)
    .reduce((bowlers, currentBowler) => {
      bowlers[currentBowler[0]] = parseFloat(
        currentBowler[1].economy.toFixed(2)
      );
      return bowlers;
    }, {});
  return top10EconomicalBowlers;
}

function filterMatchData(matches, deliveryData, season) {
  let matchesOfSeason = matches
    .filter(match => match['season'] == season)
    .map(id => id['id']);
  let deliveries = deliveryData.filter(delivery =>
    matchesOfSeason.includes(delivery['match_id'])
  );
  return deliveries;
}

module.exports = {
  matchesPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeamIn2016,
  top10EconomicalBowlersIn2015
};
