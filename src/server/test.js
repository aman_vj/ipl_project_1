var fs = require('fs');
const matchData = JSON.parse(fs.readFileSync('../data/matches.json', 'utf-8'));

function valueablePlayerPerSeason(matches) {
  let playersPerSeason = matches.reduce((players, match) => {
    if (!players[match['season']]) {
      players[match['season']] = [];
    }
    if (!players[match['season']][match['player_of_match']])
      players[match['season']][match['player_of_match']] = 1;
    else {
      players[match['season']][match['player_of_match']]++;
    }
    return players;
  }, {});
  return playersPerSeason;
}
console.log(valueablePlayerPerSeason(matchData));
