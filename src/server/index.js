var fs = require('fs');
var result = require('./ipl.js');
const matchData = JSON.parse(fs.readFileSync('../data/matches.json', 'utf-8'));
const deliveryData = JSON.parse(
  fs.readFileSync('../data/deliveries.json', 'utf-8')
);

let matchesPerYear = result.matchesPerYear(matchData);
let matchesWonPerTeamPerYear = result.matchesWonPerTeamPerYear(matchData);
let extraRunsConcededPerTeamIn2016 = result.extraRunsConcededPerTeamIn2016(
  matchData,
  deliveryData,
  2016
);
let top10EconomicalBowlersIn2015 = result.top10EconomicalBowlersIn2015(
  matchData,
  deliveryData,
  2015
);

let ans1 = JSON.stringify(matchesPerYear, null, 2);
let ans2 = JSON.stringify(matchesWonPerTeamPerYear, null, 2);
let ans3 = JSON.stringify(extraRunsConcededPerTeamIn2016, null, 2);
let ans4 = JSON.stringify(top10EconomicalBowlersIn2015, null, 2);

fs.writeFileSync('../output/matchesPerYear.json', ans1);
fs.writeFileSync('../output/matchesWonPerTeamPerYear.json', ans2);
fs.writeFileSync('../output/extraRunsConcededPerTeamIn2016.json', ans3);
fs.writeFileSync('../output/top10EconomicalBowlersIn2015.json', ans4);
